<?php
/**
 * Logger 
 *
 * Helper class
 *
<code>
        Logger::log('some title')
                ->level('error')
                ->category('default')
                ->message("some message");
</code>
 * 
 * @author vi mark <webvimark@gmail.com> 
 * @license MIT
 */
class Logger
{
        public $level = 'dirty';
        public $message = '';
        public $title = '';
        public $category = 'default';

        /**
         * log 
         * 
         * @param string $title 
         * @return self
         */
        public static function log($title = '')
        {
                $instance = new self;
                $instance->title = $title;

                return $instance;
        }

        /**
         * level 
         * 
         * @param string $level 
         * @return self
         */
        public function level($level)
        {
                $this->level = $level;

                return $this;
        }

        /**
         * message 
         * 
         * @param string $message 
         * @return self
         */
        public function message($message)
        {
                $this->message = $message;

                return $this;
        }

        /**
         * category 
         * 
         * @param string $category 
         * @return self
         */
        public function category($category)
        {
                $this->category = $category;

                return $this;
        }

        //=========== Lists ===========

        /**
        * getLevelList 
        * 
        * @return array
        */
        public static function getLevelList()
        {
                return array(
                        'error' => 0,
                        'info'  => 1,
                        'dirty' => 2,
                );
        }
        
        /**
        * getLevelValue 
        * 
        * @param string $val 
        * @return string
        */
        public static function getLevelValue($val)
        {
                $ar = self::getLevelList();
                return isset($ar[$val]) ? $ar[$val] : $val;
        }

        //----------- Lists -----------

        /**
         * __destruct 
         *
         * Save log
         */
        public function __destruct()
        {
                ULogger::saveLog($this->title, $this->message, self::getLevelValue($this->level), $this->category);
        }
}
