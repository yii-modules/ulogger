Logger
==

If instal manually then in main config:
--

```php

	'import'=>array(
                ...
                'application.modules.ULogger.models.*',
                ...
	),

	'modules'=>array(
                ...
                'ULogger',
                ...
        ),


```

If install via Composer then in main config:
--

```php

	'modules'=>array(
                ...
                'ULogger'=>array(
                        'class'=>'ym.ULogger.ULoggerModule',
                ),
                ...
        ),

```

Usage
--

```php

Logger::log('some title')
        ->level('error')
        ->category('default')
        ->message("some message");

```
