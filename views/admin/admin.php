<?php $this->breadcrumbs = array("Логи"); ?>

<?php $pageSize = Yii::app()->user->getState("pageSize",20); ?>
<h2>Логи</h2>


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'ulogger-grid',
	'dataProvider'=>$model->search(),
        'ajaxUpdate'=>false,
	'filter'=>$model,
	'columns'=>array(
                array(
                        'name'=>'id',
                        'htmlOptions'=>array(
                                'width'=>'25',
                                'class'=>'centered'
                        ),
                ),
                array(
                        'name'=>'name',
                        'value'=>'CHtml::link($data->name, array("view", "id"=>$data->id))',
                        'type'=>'raw',
                ),
                array(
                        'name'=>'active_user_name',
                        'value'=>'CHtml::value($data->user, "login")',
                ),
                array(
                        'name'=>'category',
                        'filter'=>ULogger::getCategoryList(),
                ),
                array(
                        'name'=>'level',
                        'filter'=>ULogger::getLevelList(),
                        'value'=>'ULogger::getLevelValue($data->level)',
                        'type'=>'raw',
                ),
                array(
                        'name'=>'create_date',
                        'value'=>'date("Y-m-d H:i:s", $data->create_date)',
                ),
		'ip',
                array(
                        'name'=>'is_console',
                        'filter'=>ULogger::getConsoleList(),
                        'value'=>'ULogger::getConsoleValue($data->is_console)',
                        'type'=>'raw',
                ),
		array(
			'class'=>'CButtonColumn',
                        'template'=>'{view}',
                        'header'=>CHtml::dropDownList('pageSize',$pageSize,array(20=>20,50=>50,100=>100,200=>200),array(
                                'onchange'=>"$.fn.yiiGridView.update('ulogger-grid',{ data:{pageSize: $(this).val() }})",
                                'style'=>'width:50px'
                        )),
		),
	),
        'itemsCssClass'=>'table table-hover table-striped table-bordered table-condensed',
)); ?>

<?php
$this->widget('ym.Drp.Drp', array(
        'model'=>'ULogger',
        'attribute'=>'create_date',
        'params'=>array(
                'format'=>'YYYY-MM-DD H:mm',
                'timePicker'=>true,
                'timePicker12Hour'=>false,
                'timePickerIncrement'=>5,
                'locale'=>array(
                        'fromLabel'=>Yii::t("drp","С"),
                        'toLabel'=>Yii::t("drp","По"),
                        'applyLabel' => Yii::t("drp","Принять"),
                        'cancelLabel' => Yii::t("drp","Отмена"),
                        'customRangeLabel' => Yii::t("drp","Произвольная дата"),
                        'daysOfWeek' => (Yii::app()->language == 'ru') ? array('Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс') : '',
                        'monthNames' => (Yii::app()->language == 'ru') ? array('Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июнь', 'Июль', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек') : '',
                ),
                'ranges' => array(
                        Yii::t("drp","Вчера") => array(
                                date('Y-m-d', strtotime('-1 day')), 
                                date('Y-m-d', time())
                        ),
                        Yii::t("drp","Сегодня") => array(
                                date('Y-m-d', time()), 
                                date('Y-m-d', time()) . ' 23:59'
                        ),
                        Yii::t("drp","30 дней") => array(
                                date('Y-m-d', strtotime('-1 month')), 
                                date('Y-m-d', time()) . ' 23:59'
                        ),
                        Yii::t("drp","Предыдущий месяц") => array(
                                date('Y-m-d', strtotime('first day of previous month')), 
                                date('Y-m-d', strtotime('last day of previous month')) . ' 23:59' 
                        ),
                        Yii::t("drp","Текущий месяц") => array(
                                date('Y-m-d', strtotime('first day of this month')), 
                                date('Y-m-d', time()) . ' 23:59'
                        ),
                ),
        ),
));
?>
